# 程式執行說明
## pip3 install -r requiremets.txt
## add your mongodb user, password in .env file
## 試題1
1. 進入試題1，並執行test_1.py
2. 會看到執行時間&回傳執行結果
## 試題2
1. 透過docker_deploy.sh執行docker指令並建立mongo DB所db&collection
1. docker_image.png為docker pull的結果
3. docker_containers_with_shell_screen.png為透過terminal執行後的結果。
4. 進入試題2，並執行mongo_db.py，會執行試題1的結果並將其寫入創建好的mongoDB，並印出id

