import pandas as pd
import json
import time
import sys
import os
import pathlib
from dotenv import *
load_dotenv()
sys.path.append(str(pathlib.Path().resolve().parent))
sys.path.append('.')

def data_process():
    df = pd.read_csv('CSV2JSON.csv')
    result = []
    start = time.time()
    # for loop each rows
    for row in df.itertuples():
        member_id = row.member_id
        tag_name = row.tag_name
        detail_name = row.detail_name
        detail_value = row.detail_value
        # 檢查result中是否已存在相同的member_id
        existing_member = next((item for item in result if item["_id"] == member_id), None)
        if existing_member:
            # 檢查是否已存在相同的tag_name
            existing_tag = next((item for item in existing_member["tags"] if item["tag_name"] == tag_name), None)
            if existing_tag:
                # 在tag中加上detail
                existing_tag["detail"].append({
                    "detail_name": detail_name,
                    "detail_value": detail_value
                })
            else:
                # 在現有的member中加新的tag
                existing_member["tags"].append({
                    "tag_name": tag_name,
                    "detail": [{
                        "detail_name": detail_name,
                        "detail_value": detail_value
                    }]
                })
        else:
            # 在result添加新的member
            result.append({
                "_id": member_id,
                "member_id": member_id,
                "tags": [{
                    "tag_name": tag_name,
                    "detail": [{
                        "detail_name": detail_name,
                        "detail_value": detail_value
                    }]
                }]
            })
    print(f"總共耗時: {time.time() - start}")
    print(json.dumps(result, indent=4))
    # print(result)
    return result

if __name__ == "__main__":
    data_process()