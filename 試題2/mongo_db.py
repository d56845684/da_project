
from pymongo import MongoClient
from dotenv import *
import os 
import sys
import pathlib
load_dotenv()
sys.path.append(str(pathlib.Path().resolve().parent))
from 試題1.test_1 import *
ACCOUND = os.getenv('mongo_db_account')
PASSWORD = os.getenv('mongo_db_password')
CLIENT = MongoClient(f"mongodb://{ACCOUND}:{PASSWORD}@127.0.0.1:27017/cathy_test?directConnection=true&serverSelectionTimeoutMS=2000&appName=mongosh+1.8.2")
DB = CLIENT["cathy_test"]
COLLECTION = DB["customers"]


def insertdata2dbmany(documents):
    """
    新增多筆資料到db
    documents: List
    """
    log = COLLECTION.insert_many(documents)
    print("插入成功，插入的文件 ID 為:", log.inserted_ids)

if __name__ == "__main__":
    doc = data_process()
    insertdata2dbmany(doc)
